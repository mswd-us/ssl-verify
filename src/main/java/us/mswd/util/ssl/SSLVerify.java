package us.mswd.util.ssl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility to verify certificate(s) are valid within the configured (or default)
 * trustStore.
 * 
 * @author Kendal Montgomery <mswd.us@gmail.com>
 */
public class SSLVerify {

	private static final Logger _log = LoggerFactory.getLogger(SSLVerify.class);

	private static final String CERT_OPT = "cert";
	private static final String URL_OPT = "url";
	private static final String VERBOSE_OPT = "verbose";

	private static final String RSA_ALGORITHM = "RSA";

	/**
	 * Program entry.
	 * 
	 * @param args
	 *            command line arguments
	 */
	public static void main(final String... args) {
		final Options options = new Options();
		final CommandLineParser parser = new DefaultParser();

		final Option certOption = Option.builder().argName("cert-path").longOpt("cert").hasArg()
				.desc("the certificate to verify against the trustStore").type(File.class).build();
		final Option urlOption = Option.builder().argName("url").longOpt("url").hasArg()
				.desc("the url to user for an https service to verify the certificate against our trustStore").build();
		final Option verboseOption = Option.builder().longOpt("verbose").desc("verbose mode").build();

		options.addOption(certOption);
		options.addOption(urlOption);
		options.addOption(verboseOption);

		try {
			final CommandLine cl = parser.parse(options, args);
			if (!cl.hasOption(CERT_OPT) && !cl.hasOption(URL_OPT)) {
				_log.error("one option is required");
				writeUsageError(options);
				System.exit(1);
			}

			if (cl.hasOption(CERT_OPT)) {
				final File certFile = (File) cl.getParsedOptionValue(CERT_OPT);
				if (!certFile.exists()) {
					_log.error("certificate file {} does not exist.", certFile.getAbsolutePath());
					System.exit(2);
				}
				try {
					final X509TrustManager tm = getDefaultTrustManager();
					final ArrayList<X509Certificate> certs = new ArrayList<>();
					final X509Certificate cert = (X509Certificate) CertificateFactory.getInstance("X.509")
							.generateCertificate(new FileInputStream(certFile));
					certs.add(cert);
					tm.checkServerTrusted(certs.toArray(new X509Certificate[] {}), RSA_ALGORITHM);
				} catch (final CertificateException | FileNotFoundException ex) {
					if (cl.hasOption(VERBOSE_OPT)) {
						_log.warn("certificate error.", ex);
					} else {
						_log.warn("certificate error: {}", ex.getMessage());
					}
					System.exit(3);
				}
			} else if (cl.hasOption(URL_OPT)) {
				final String url = cl.getOptionValue(URL_OPT);
				if (cl.hasOption(VERBOSE_OPT))
					_log.debug("URL = {}", url);
				if (!url.toLowerCase().startsWith("https://")) {
					_log.info("not a secure URL, no validation being done.");
					System.exit(0);
				}
				try {
					final URL u = new URL(url);
					final HttpsURLConnection urlConn = (HttpsURLConnection) u.openConnection();
					urlConn.connect();
					final ArrayList<X509Certificate> certList = new ArrayList<>();
					for (final Certificate cert : urlConn.getServerCertificates()) {
						if (cert instanceof X509Certificate) {
							certList.add((X509Certificate) cert);
						}
					}
					final X509TrustManager tm = getDefaultTrustManager();
					tm.checkServerTrusted(certList.toArray(new X509Certificate[] {}), RSA_ALGORITHM);
				} catch (final Exception ex) {
					if (cl.hasOption(VERBOSE_OPT)) {
						_log.warn("certificate error", ex);
					} else {
						_log.warn("certificate error: {}", ex.getMessage());
					}
					System.exit(3);
				}
			}
			_log.info("SUCCESS");
			System.exit(0);
		} catch (final ParseException e) {
			_log.error("invalid command line options", e);
			writeUsageError(options);
			System.exit(1);
		}
	}

	private static X509TrustManager getDefaultTrustManager() {
		TrustManagerFactory tmFactory;
		try {
			tmFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm(), "SunJSSE");
			tmFactory.init((KeyStore) null);
			for (final TrustManager tm : tmFactory.getTrustManagers()) {
				if (tm instanceof X509TrustManager) {
					return (X509TrustManager) tm;
				}
			}
		} catch (final Exception ex) {
			_log.warn("could not find default trust manager.", ex);
		}
		return null;
	}

	private static PrintWriter sysErrWrapper() {
		return new PrintWriter(System.out);
	}

	private static final void writeUsageError(final Options options) {
		try (final PrintWriter errWriter = sysErrWrapper()) {
			new HelpFormatter().printHelp(100, "ssl-verify", null, options,
					"\n  note: specify trustStore location by passing -Djavax.net.ssl.trustStore=<path> jvm argument",
					true);
			errWriter.flush();
		}
	}

}
